#!/bin/sh

##############################################################################
#  This file is part of Retaliator.                                          #
#                                                                            #
#  Copyright (C) 2012, 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>      #
#                                                                            #
#  Retaliator is free software: you can redistribute it and/or modify        #
#  it under the terms of the GNU Affero General Public License version 3 as  #
#  published by the Free Software Foundation on 19 November 2007.            #
#                                                                            #
#  Retaliator is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Affero General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Affero General Public License  #
#  along with Retaliator.  If not, see <http://www.gnu.org/licenses/>.       #
##############################################################################

# Description: Launcher for the flight simulator known as F29 Retaliator

# Emulator setup
db=dosbox

# SHA setup
ss=shasum

# Global setup
rom=$PWD/../rom
doc=$PWD/../doc
sha=$PWD/../sha
gdr=$PWD/../etc/$db/retaliator.conf
gdu=$PWD/../etc/$db/user.conf
usr=$HOME/.retaliator

# Local setup
dd=$usr/mnt
dr=$gdr
du=../$db.conf
dg=../autodb.conf

# Default values
port=3881
slot=F.YOUNG

# Script information
script=$(basename "$0")
name='Retaliator'
help="Description:
  Launcher for the flight simulator known as F29 Retaliator
Usage:
  $script [option [value]]... [-- [dosbox_option [value]]...]
Documentaion options:
  -h, --help                   Print usage information.
  -v, --version                Print version information.
  -m, --manual                 View original typewritten manual.
Multiplayer options:
  -r, --remote-host <host>     Host to join on
  -s, --service <port> ($port)  Port to host on or to join on in case of -r
Slot options:
  -l, --list                   List all slots.
  -e, --edit                   Edit comment of picked slot via vim/vi.
  -k, --kill                   Kill log file and comment of picked slot.
  -n, --name <name>            Name picked slot as <name>.
  -c, --copy <name>            Copy picked slot to <name>.
  -o, --open <name>            Open slot <name> replacing picked one.
  -p, --pick <name>            Pick slot <name> Retaliator will write to.
  -d, --drop <name>            Drop slot <name>.
ROM options:
  -i, --inquire                Validate Retaliator ROM against known versions."
usage="Try \`$script --help' for more information."
version='1.4.0'
info="$script-$version

Copyright (C) 2012, 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>

$name is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation on 19 November 2007.

$name is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with $name.  If not, see <http://www.gnu.org/licenses/>."

# Declare slot functions.
isName() {
	if [ "$(echo "$1" | tr -dc '/')" ] || [ $(echo "$1" | wc -l) -gt 1 ]; then
		echo >&2 "Neither '/' nor '\\\n' is allowed."
		exit 1
	fi
}
isSlot() {
	isName "$1"
	if [ ! -f "../log/$1" ]; then
		echo >&2 "There is no slot '$1'."
		exit 2
	fi
}
noSlot() {
	isName "$1"
	if [ -f "../log/$1" ] || [ -f "../txt/$1" ]; then
		echo >&2 "Slot '$1' does already exist."
		exit 3
	fi
}
getSlot() {
	basename "$(readlink 2>/dev/null RETAL.LOG)" || {
		echo >&2 'Cannot get picked slot.'
		exit 4
	}
}
setSlot() {
	ln >/dev/null 2>&1 -sf "../log/$1" RETAL.LOG || {
		echo >&2 'Cannot pick slot '$1'.'
		exit 5
	}
}
defSlot() {
	if [ "$1" ]; then
		echo "$1"
	else
		getSlot
	fi
}

# Setup user and enter its emulator root.
mkdir >/dev/null 2>&1 -p "$dd" "$usr/log" "$usr/txt" || {
	echo >&2 "Cannot create directory structure for user."
	exit 6
}
cd >/dev/null 2>&1 "$dd" || {
	echo >&2 "Cannot enter emulator root."
	exit 7
}
ln >/dev/null 2>&1 -nsf "$rom" RETAL || {
	echo >&2 "Cannot link to Retaliator ROM."
	exit 8
}
if [ ! -f "$du" ]; then
	sed 2>/dev/null '1 s/^#.*/# Actual user specific configuration of dosbox/' "$gdu" > "$du" || {
		echo >&2 "Cannot create user specific dosbox configuration."
		exit 9
	}
fi
if [ ! "$(ls -A ../log)" ]; then
	echo 2>/dev/null -n > "../log/$slot" &&
	echo 2>/dev/null -n > "../txt/$slot" || {
		echo >&2 "Cannot create default slot."
		exit 10
	}
	ln >/dev/null 2>&1 -sf "../log/$slot" RETAL.LOG || {
		echo >&2 "Cannot pick default slot."
		exit 11
	}
fi
if [ ! -h RETAL.LOG ]; then
	setSlot "$slot"
fi
if [ -f RETAL/RETAL.LOG ] || [ -h RETAL/RETAL.LOG ]; then
	echo >&2 'Please remove RETAL.LOG from Retaliator ROM.'
	exit 12
fi
if [ ! -f RETAL/X.EXE ] || [ ! -f RETAL/RETAL.00 ] ||[ ! -f RETAL/RETAL.01 ]; then
	echo >&2 'Please install a valid Retaliator ROM.'
	exit 13
fi

# Declare options.
short='hvmr:s:lekn:c:o:p:d:i'
long='help,version,manual,remote-host:,service:,list,edit,kill,name:,copy:,open:,pick:,drop:,inquire'

# Parse arguments.
getopt=$(getopt -n "$script" -l "$long" "$short" "$@")
if [ $? -ne 0 ]; then
	echo "$usage"
	exit 14
fi
eval set -- $getopt

# Interpret arguments.
while [ $# -gt 0 ]; do option=$1; shift; value=$1; case $option in
	-h|--help)
		echo "$help"
		exit 0
	;;
	-v|--version)
		echo "$info"
		exit 0
	;;
	-m|--manual)
		xdg-open 2>/dev/null "$doc/manual/manual.pdf" ||
		xdg-open 2>/dev/null "$doc/manual/manual.html" ||
		w3m 2>/dev/null "$doc/manual/manual.html" ||
		less 2>/dev/null "$doc/manual/manual.txt" || {
			echo >&2 'Cannot view manual.'
			exit 15
		}
		exit=0
	;;
	-r|--remote-host) shift
		host=$value
		mode=1
	;;
	-s|--service) shift
		if [ "$value" ]; then
			port=$value
		fi
		mode=1
	;;
	-l|--list)
		stat -c '%s %Y %n' ../log/* | sort -k 1nr -k 2nr | while read line; do
			slot=$(basename "$(echo "$line" | sed 's/^[^ ]\+ [^ ]\+ //')")
			icon='*'
			if [ "$(getSlot)" != "$slot" ]; then
				icon=' '
			fi
			echo "$icon$slot"
			cat 2>/dev/null "../txt/$slot" | sed 's/\(.*\)/   \1/'
		done
		exit 0
	;;
	-e|--edit)
		slot=$(getSlot)
		vim 2>/dev/null -c start "../txt/$slot" ||
		vi 2>/dev/null -c start "../txt/$slot" || {
			echo >&2 "Cannot edit comment of picked slot '$value'."
			exit 16
		}
		exit=0
	;;
	-k|--kill)
		slot=$(getSlot)
		echo 2>/dev/null -n > "../log/$slot" &&
		echo 2>/dev/null -n > "../txt/$slot" || {
			echo >&2 "Cannot kill picked slot '$slot'."
			exit 17
		}
		exit=0
	;;
	-n|--name) shift
		value=$(defSlot "$value")
		noSlot "$value"
		slot=$(getSlot)
		mv >/dev/null 2>&1 "../log/$slot" "../log/$value" &&
		mv >/dev/null 2>&1 "../txt/$slot" "../txt/$value" || {
			echo >&2 "Cannot name slot as '$value'."
			exit 18
		}
		setSlot "$value"
		exit=0
	;;
	-c|--copy) shift
		value=$(defSlot "$value")
		slot=$(getSlot)
		cp >/dev/null 2>&1 "../log/$slot" "../log/$value" &&
		cp >/dev/null 2>&1 "../txt/$slot" "../txt/$value" || {
			echo >&2 "Cannot copy slot to '$value'."
			exit 19
		}
		exit=0
	;;
	-o|--open) shift
		value=$(defSlot "$value")
		isSlot "$value"
		slot=$(getSlot)
		if [ ! "$value" = "$slot" ]; then
			cp >/dev/null 2>&1 "../log/$value" "../log/$slot" &&
			cp >/dev/null 2>&1 "../txt/$value" "../txt/$slot" || {
				echo >&2 "Cannot open slot '$value'."
				exit 20
			}
		fi
		exit=0
	;;
	-p|--pick) shift
		value=$(defSlot "$value")
		isSlot "$value"
		setSlot "$value"
		exit=0
	;;
	-d|--drop) shift
		value=$(defSlot "$value")
		isSlot "$value"
		if [ $(ls -A ../log | wc -l) -eq 1 ]; then
			echo >&2 "There should be at least one slot left."
			exit 21
		fi
		if [ "$(getSlot)" = "$value" ]; then
			echo >&2 "Cannot drop picked slot."
			exit 22
		fi
		rm >/dev/null 2>&1 "../log/$value" &&
		rm >/dev/null 2>&1 "../txt/$value" || {
			echo >&2 "Cannot drop slot '$value'."
			exit 23
		}
		exit=0
	;;
	-i|--inquire)
		cd >/dev/null 2>&1 "$rom" || {
			echo 1>&2 'Cannot enter Retaliator ROM.'
			exit 24
		}
		command >/dev/null 2>&1 -v $ss || {
			echo >&2 "Requiring '$ss'."
			exit 25
		}
		version='Unknown'
		for line in $(ls "$sha"); do
			$ss >/dev/null 2>&1 -c "$sha/$line"
			case $? in
				0)
					version=$(echo "$line" | sed 's/_/ /g')
					break
				;;
				2)
					echo >&2 "Cannot inquire Retaliator ROM '$line'."
					exit 26
				;;
			esac
		done
		echo "Retaliator ROM: $version"
		exit 0
	;;
	--)
		break
	;;
esac done

# Exit on demand.
if [ "$exit" ]; then
	exit $exit
fi

# Determine game mode.
if [ "$mode" ]; then
	mode='nullmodem'
	if [ "$host" ]; then
		mode="$mode server:$host port:$port"
	else
		mode="$mode port:$port"
	fi
else
	mode='dummy'
fi

# Original resolution
bx=320
by=200

# Extends the dosbox scalers, normal2x and normal3x, by calculating the
# maximum integral multiple of the original resolution fitting into the
# maximum device resolution.
normalix() {
	sx=$(echo "$1" | cut -d 'x' -f 1)
	sy=$(echo "$1" | cut -d 'x' -f 2)
	px=$(($2+0))
	py=$(($3+0))
	sx=$(($sx-$px))
	sy=$(($sy-$py))
	ix=$(($sx/$bx))
	iy=$(($sy/$by))
	if [ "$ix" -gt "$iy" ]; then
		ix=$iy
	else
		iy=$ix
	fi
	echo "$(($ix*$bx))x$(($iy*$by))"
}

# Extracts resolution, line by line.
xcrossy() {
	sed -n 's/\s*\([0-9]\+\)x\([0-9]\+\).*/\1x\2/p'
}

# Determines minimum of multiple modes.
minimum() {
	for cm in $1; do
		x=$(echo "$cm" | cut -d 'x' -f 1)
		y=$(echo "$cm" | cut -d 'x' -f 2)
		xy=$(($x*$y))
		if [ ! "$ls" ] || [ $xy -lt $ls ]; then
			ls=$xy
			lm=$cm
		fi
	done
	if [ "$lm" ]; then
		echo $lm
	else
		return 1
	fi
}

# Calculate frame paddings by defining pseudo borders and decorative title.
bs=10
ds=40
px=$(($bs+$bs))
py=$(($ds+$bs))

# Determines maximum device resolution (the minumum of multiple maximums).
maximum() {
	modes=$(xrandr 2>/dev/null --current | grep '   ')
	minimum "$(echo "$modes" | grep '+' | xcrossy)" ||
	minimum "$(echo "$modes" | grep '*' | xcrossy)" ||
	echo $(($bx+$bx+$px))x$(($by+$by+$py))
}

# Calculate preferred or current or minimum device resolution, respectively.
sz=$(maximum)

# Calculate window resolution by subtracting its frame paddings.
rw=$(normalix "$sz" $px $py)

# Calculate full screen resolution.
rf=$(normalix "$sz")

# Generic configuration
generic="# Temporary multiplayer and resolution configuration of dosbox
[serial]
serial1=$mode
serial2=dummy
serial3=disabled
serial4=disabled
[sdl]
output=openglnb
windowresolution=$rw
fullresolution=$rf
[render]
scaler=none"

# Require emulator.
command >/dev/null 2>&1 -v $db || {
	echo >&2 "Requiring '$db'."
	exit 27
}

# Apply generic configuration.
echo 2>/dev/null "$generic" > "$dg" || {
	echo >&2 "Cannot create generic configuration of dosbox."
	exit 28
}

# Launch Retaliator.
$db >/dev/null 2>&1 -conf "$dg" -conf "$dr" -conf "$du" \
-c 'MOUNT C "."' \
-c 'CONFIG -securemode' \
-c 'C:' \
-c 'RETAL\X.EXE' \
-c 'EXIT' \
$@ || {
	echo >&2 "Cannot launch '$db'."
	rm >/dev/null 2>&1 "$dg"
	exit 29
}
rm >/dev/null 2>&1 "$dg"
