##############################################################################
#  This file is part of Retaliator.                                          #
#                                                                            #
#  Copyright (C) 2012, 2013  Rouven Spreckels  <nevuor@nevux.org>            #
#                                                                            #
#  Retaliator is free software: you can redistribute it and/or modify        #
#  it under the terms of the GNU Affero General Public License version 3 as  #
#  published by the Free Software Foundation on 19 November 2007.            #
#                                                                            #
#  Retaliator is distributed in the hope that it will be useful,             #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Affero General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Affero General Public License  #
#  along with Retaliator.  If not, see <http://www.gnu.org/licenses/>.       #
##############################################################################

_retaliator()
{
	local cur=${COMP_WORDS[COMP_CWORD]}

	case "${COMP_WORDS[COMP_CWORD-1]}" in
		-n|--name|\
		-c|--copy|\
		-o|--open|\
		-p|--pick|\
		-d|--drop)
			local IFS=$'\n'
			COMPREPLY=( $(cd "$HOME/.retaliator/log" && compgen -f -- $cur) )
		;;
		-r|--remote-host)
			COMPREPLY=( $(compgen -W "$(cat 2>/dev/null /etc/hosts | sed 's/#.*//')" -- $cur) )
		;;
		-s|--service)
			COMPREPLY=( $(compgen -W "3881" -- $cur) )
		;;
		*)
			COMPREPLY=( $(compgen -W '\
				-h --help \
				-v --version \
				-m --manual \
				-r --remote-host \
				-s --service \
				-l --list \
				-e --edit \
				-k --kill \
				-n --name \
				-c --copy \
				-o --open \
				-p --pick \
				-d --drop \
				-i --inquire \
			' -- $cur) )
		;;
	esac
}

complete -o filenames -F _retaliator retaliator
