<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
  -  This file is part of Retaliator.                                          -
  -                                                                            -
  -  Copyright (C) 2013, 2014  Rouven Spreckels  <n3vu0r@nevux.org>            -
  -                                                                            -
  -  Retaliator is free software: you can redistribute it and/or modify        -
  -  it under the terms of the GNU Affero General Public License version 3 as  -
  -  published by the Free Software Foundation on 19 November 2007.            -
  -                                                                            -
  -  Retaliator is distributed in the hope that it will be useful,             -
  -  but WITHOUT ANY WARRANTY; without even the implied warranty of            -
  -  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             -
  -  GNU Affero General Public License for more details.                       -
  -                                                                            -
  -  You should have received a copy of the GNU Affero General Public License  -
  -  along with Retaliator.  If not, see <http://www.gnu.org/licenses/>.       -
  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes" indent="yes"/>
	<xsl:template match="/book">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<link href="manual.css" media="screen" rel="stylesheet" title="style" type="text/css"/>
				<link rel="icon" type="image/png" href="manual.png"/>
				<title><xsl:value-of select="bookinfo/title"/></title>
			</head>
			<body>
				<h1><xsl:value-of select="bookinfo/title"/></h1>
				<p><xsl:value-of select="bookinfo/date"/></p>
				<xsl:if test="part">
					<ul>
						<xsl:apply-templates select="part" mode="toc"/>
					</ul>
				</xsl:if>
				<xsl:if test="part">
					<ul>
						<xsl:apply-templates select="part" mode="bdy"/>
					</ul>
				</xsl:if>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="part" mode="toc">
		<li>
			<h2><a href="#{generate-id(title)}"><xsl:value-of select="title"/></a></h2>
			<xsl:if test="sect1">
				<ul>
					<xsl:apply-templates select="sect1" mode="toc"/>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="part" mode="bdy">
		<li>
			<h2 id="{generate-id(title)}"><xsl:value-of select="title"/></h2>
			<xsl:apply-templates select="para|literallayout"/>
			<xsl:if test="sect1">
				<ul>
					<xsl:apply-templates select="sect1" mode="bdy"/>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="sect1" mode="toc">
		<li>
			<h3><a href="#{generate-id(title)}"><xsl:value-of select="title"/></a></h3>
			<xsl:if test="sect2">
				<ul>
					<xsl:apply-templates select="sect2" mode="toc"/>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="sect1" mode="bdy">
		<li>
			<h3 id="{generate-id(title)}"><xsl:value-of select="title"/></h3>
			<xsl:apply-templates select="para|literallayout"/>
			<xsl:if test="sect2">
				<ul>
					<xsl:apply-templates select="sect2" mode="bdy"/>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="sect2" mode="toc">
		<li>
			<h4><a href="#{generate-id(title)}"><xsl:value-of select="title"/></a></h4>
			<xsl:if test="sect3">
				<ul>
					<xsl:apply-templates select="sect3" mode="toc"/>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="sect2" mode="bdy">
		<li>
			<h4 id="{generate-id(title)}"><xsl:value-of select="title"/></h4>
			<xsl:apply-templates select="para|literallayout"/>
			<xsl:if test="sect3">
				<ul>
					<xsl:apply-templates select="sect3" mode="bdy"/>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="sect3" mode="toc">
		<li>
			<h5><a href="#{generate-id(title)}"><xsl:value-of select="title"/></a></h5>
		</li>
	</xsl:template>
	<xsl:template match="sect3" mode="bdy">
		<li>
			<h5 id="{generate-id(title)}"><xsl:value-of select="title"/></h5>
			<xsl:apply-templates select="para|literallayout"/>
		</li>
	</xsl:template>
	<xsl:template match="para">
		<p><xsl:value-of select="."/></p>
	</xsl:template>
	<xsl:template match="literallayout">
		<pre><xsl:value-of select="."/></pre>
	</xsl:template>
</xsl:stylesheet>
